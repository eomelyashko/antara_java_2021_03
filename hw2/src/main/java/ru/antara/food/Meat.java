package ru.antara.food;

public class Meat extends Food {

    public Meat() {
        this(5);
    }

    public Meat(int value) {
        this("какое-то мясо", value);
    }

    public Meat(String name, int value) {
        this.name = name;
        this.energyValue = value;
    }
}
