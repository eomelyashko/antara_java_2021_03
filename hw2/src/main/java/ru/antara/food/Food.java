package ru.antara.food;

public abstract class Food {
    protected String name;
    protected int energyValue;

    public String getName() {
        return name;
    }

    public int getEnergyValue() {
        return energyValue;
    }
}
