package ru.antara.food;

public class Grass extends Food {

    public Grass() {
        this(5);
    }

    public Grass(int value) {
        this("какая-то зелень", value);
    }

    public Grass(String name, int value) {
        this.name = name;
        this.energyValue = value;
    }
}
