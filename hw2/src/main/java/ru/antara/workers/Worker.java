package ru.antara.workers;

import ru.antara.animals.Animal;
import ru.antara.animals.Voice;
import ru.antara.food.Food;

public class Worker {
    public void feed(Animal animal, Food food) {
        animal.eat(food);
    }

    public String getVoice(Voice animal) {
        return animal.voice();
    }
}
