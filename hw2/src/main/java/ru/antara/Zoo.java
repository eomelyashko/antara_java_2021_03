package ru.antara;

import ru.antara.animals.*;
import ru.antara.food.Grass;
import ru.antara.food.Meat;
import ru.antara.workers.Worker;

public class Zoo {
    public static void main(String[] args) {
        /* Животные */
        Bear bear = new Bear();
        Duck duck = new Duck();
        Giraffe giraffe = new Giraffe();
        Tiger tiger = new Tiger();
        Zander zander = new Zander();
        Zebra zebra = new Zebra();

        /* Еда */
        Grass grass = new Grass();
        Meat meat = new Meat();

        /* Работник зоопарка */
        Worker vasya = new Worker();
        vasya.feed(bear, grass);
        vasya.feed(duck, grass);
        vasya.feed(giraffe, meat);
//        vasya.getVoice(zander); ошибка компиляции
        vasya.getVoice(duck);
        vasya.getVoice(zebra);
        vasya.getVoice(tiger);

        /* Пруд */
        Swim[] pond = new Swim[10];
        pond[0] = new Duck();
        pond[1] = new Duck();
        pond[2] = new Duck();
        pond[3] = new Zander();
        pond[4] = new Zander();
        pond[5] = new Zander();
        pond[6] = new Zander();
        pond[7] = new Tiger();
        pond[8] = new Bear();
        pond[9] = new Zebra();

        for (Swim animal : pond) {
            animal.swim();
        }
    }
}
