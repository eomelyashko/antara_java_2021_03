package ru.antara.animals;

public class Tiger extends Carnivorous implements Run, Swim, Voice {

    public Tiger() {
        this(10);
    }

    public Tiger(int fed) {
        this.fed = fed;
    }

    @Override
    public boolean run() {
        if (!isFed()) {
            return false;
        }
        this.fed--;
        return true;
    }

    @Override
    public boolean swim() {
        if (!isFed()) {
            return false;
        }
        this.fed--;
        return true;
    }

    @Override
    public String voice() {
        if (!isFed()) {
            return "ХОЧУ ЕСТЬ!!!";
        }
        this.fed--;
        return "Рррразве не замечательный голос!";
    }
}
