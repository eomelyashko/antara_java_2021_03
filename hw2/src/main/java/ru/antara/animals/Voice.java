package ru.antara.animals;

public interface Voice {
    String voice();
}
