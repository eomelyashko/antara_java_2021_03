package ru.antara.animals;

public interface Fly {
    boolean fly();
}
