package ru.antara.animals;

import ru.antara.food.Food;
import ru.antara.food.Meat;

public abstract class Carnivorous extends Animal {
    @Override
    public void eat(Food food) {
        if (!(food instanceof Meat)) {
            System.out.println("Используйте мясо)");
        }
        this.fed += food.getEnergyValue();
    }
}
