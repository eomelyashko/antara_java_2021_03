package ru.antara.animals;

public interface Run {
    boolean run();
}
