package ru.antara.animals;

public class Bear extends Carnivorous implements Run, Swim, Voice {

    public Bear() {
        this(10);
    }

    public Bear(int fed) {
        this.fed = fed;
    }

    @Override
    public boolean run() {
        if (!isFed()) {
            return false;
        }
        this.fed--;
        return true;
    }

    @Override
    public boolean swim() {
        if (!isFed()) {
            return false;
        }
        this.fed--;
        System.out.println(this.name + " плывет");
        return true;
    }

    @Override
    public String voice() {
        if (!isFed()) {
            return "ХОЧУ ЕСТЬ!!!";
        }
        this.fed--;
        return "СЕЙЧАС Я ПОКАЖУ ТЕБЕ ГОЛОС, балалайку только отложу!!!!";
    }
}
