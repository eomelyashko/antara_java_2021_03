package ru.antara.animals;

public class Duck extends Herbivore implements Fly, Swim, Run, Voice {

    public Duck() {
        this(10);
    }

    public Duck(int fed) {
        this.fed = fed;
    }

    @Override
    public boolean fly() {
        if (!isFed()) {
            return false;
        }
        this.fed--;
        return true;
    }

    @Override
    public boolean run() {
        if (!isFed()) {
            return false;
        }
        this.fed--;
        return true;
    }

    @Override
    public boolean swim() {
        if (!isFed()) {
            return false;
        }
        this.fed--;
        return true;
    }

    @Override
    public String voice() {
        if (!isFed()) {
            return "КРЯЧУ ЕСТЬ!!!";
        }
        this.fed--;
        return "Кряксте!";
    }
}
