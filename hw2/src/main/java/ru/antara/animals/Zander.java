package ru.antara.animals;

public class Zander extends Carnivorous implements Swim {

    public Zander() {
        this(10);
    }

    public Zander(int fed) {
        this.fed = fed;
    }

    @Override
    public boolean swim() {
        if (!isFed()) {
            return false;
        }
        this.fed--;
        return true;
    }
}
