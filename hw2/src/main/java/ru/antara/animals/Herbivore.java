package ru.antara.animals;

import ru.antara.food.Food;
import ru.antara.food.Grass;

public abstract class Herbivore extends Animal {
    @Override
    public void eat(Food food) {
        if (!(food instanceof Grass)) {
            System.out.println("Воспользуйтесь зеленью)");
        }
        this.fed += food.getEnergyValue();
    }
}
