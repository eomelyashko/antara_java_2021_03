package ru.antara.animals;

public class Giraffe extends Herbivore implements Run {

    public Giraffe() {
        this(10);
    }

    public Giraffe(int fed) {
        this.fed = fed;
    }

    @Override
    public boolean run() {
        if (!isFed()) {
            return false;
        }
        this.fed--;
        return true;
    }
}
