package ru.antara.animals;

import ru.antara.food.Food;

public abstract class Animal {
    protected int fed;
    protected String name;

    public abstract void eat(Food food);

    public int getFed() {
        return fed;
    }

    public boolean isFed() {
        if (!(fed > 0)) {
            System.out.println("пора кормить!");
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return name;
    }
}
