package ru.antara.animals;

public class Zebra extends Herbivore implements Run, Voice, Swim {

    public Zebra() {
        this(10);
    }

    public Zebra(int fed) {
        this.fed = fed;
    }

    @Override
    public boolean run() {
        if (!isFed()) {
            return false;
        }
        this.fed--;
        return true;
    }

    @Override
    public boolean swim() {
        if (!isFed()) {
            return false;
        }
        this.fed--;
        return true;
    }

    @Override
    public String voice() {
        if (!isFed()) {
            return "КОРМИ!!!";
        }
        this.fed--;
        return "И-го-го";
    }
}
