package ru.antara;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.antara.repository.SearchAdvert;

import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) {

        System.setProperty("webdriver.chrome.driver", "C:\\webdriver\\chromedriver.exe");

        WebDriver driver = new ChromeDriver();

        driver.get("https://www.avito.ru/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        SearchAdvert advertObject = PageFactory.initElements(driver, SearchAdvert.class);
        /*Категории*/
        advertObject.selectCategory("Оргтехника и расходники");

        /*Поле поиска*/
        advertObject.searchField("Принтер");

        /* Регион */
        WebDriverWait waitDriver = new WebDriverWait(driver, 10);
        advertObject.clickRegionElement("Владивосток", waitDriver);

        /* Список результатов */

        /* Кнопка показать */
        advertObject.clickSearchButton();

        /* Чекбокс */
        advertObject.selectCheckbox(driver);

        /* Кнопка показать */
        advertObject.clickSearchButton();

        /* Сортировка */
        advertObject.sortedItem("Дороже");

        advertObject.printResultForNumber(driver, 3);


        driver.quit();
    }
}
