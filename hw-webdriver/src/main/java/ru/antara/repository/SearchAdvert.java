package ru.antara.repository;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SearchAdvert {
    @FindBy(id = "category")
    private WebElement categorySelector;

    @FindBy(xpath = "//input[@data-marker='search-form/suggest']")
    private WebElement searchField;

    @FindBy(xpath = "//div[@data-marker='search-form/region']")
    private WebElement regionDiv;

    @FindBy(xpath = "//div[@data-marker='popup-location/region']")
    private WebElement popupFieldRegion;

    @FindBy(xpath = "//div[@data-marker='popup-location/region']/ul/li[1]")
    private WebElement listRegionResult;

    @FindBy(xpath = "//span[contains(text(), 'Показать')]/parent::*")
    private WebElement searchButton;

    @FindBy(xpath = "//input[contains(@data-marker, 'delivery-')]")
    private WebElement deliveryCheckbox;

    @FindBy(xpath = "//div[contains(@class,'sort-select')]/select")
    private WebElement sortedSelector;

    @FindBy(xpath = "//div[@data-marker='catalog-serp']")
    private WebElement listAdvert;

    public void selectCategory(String category) {
        this.categorySelector.click();
        new Select(categorySelector).selectByVisibleText(category);
    }

    public void searchField(String text) {
        this.searchField.sendKeys(text);
    }

    public void clickRegionElement(String region, WebDriverWait wait) {
        this.regionDiv.click();
        this.popupFieldRegion.findElement(By.xpath("./input")).sendKeys(region);

        wait.until(ExpectedConditions.textToBePresentInElement(popupFieldRegion.findElement(By.xpath("..//span")), region));
        listRegionResult.click();
    }

    public void selectCheckbox(WebDriver driver) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].scrollIntoView(true)", this.deliveryCheckbox);

        if (!this.deliveryCheckbox.isSelected()) {
            this.deliveryCheckbox.findElement(By.xpath(".//..")).click();
        }
    }

    public void clickSearchButton() {
        this.searchButton.click();
    }

    public void sortedItem(String value) {
        new Select(sortedSelector).selectByVisibleText(value);
    }

    public void printResultForNumber(WebDriver driver, int number) {
        List<WebElement> adverts = this.listAdvert.findElements(By.xpath("./div[@data-marker='item'][position()<=" + number + "]"));

        for (WebElement e : adverts) {
            String title = e.findElement(By.xpath(".//a[@data-marker='item-title']")).getAttribute("title");
            String price = e.findElement(By.xpath(".//span[@data-marker='item-price']")).getText();
            System.out.println(title + " - " + price);
        }
    }
}

