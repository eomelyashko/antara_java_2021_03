package ru.antara;

import ru.antara.animals.*;
import ru.antara.aviary.Aviary;
import ru.antara.food.Grass;

public class Main {
    public static void main(String[] args) {
        /* Вольер для хищников */
        Aviary<Carnivorous> aviary = new Aviary<>(AviarySize.MEDIUM);
        Tiger bear1 = new Tiger("Диего", 25);
        Bear bear2 = new Bear("Кода", 15);
        aviary.addToAviary(bear1);
        aviary.addToAviary(bear2); // возникает исключение WrongAviarySizeException()

        /* Вольер для хищников */
        Aviary<Herbivore> aviary1 = new Aviary<>(AviarySize.SMALL);
        Duck duck1 = new Duck("Скрудж", 10);
        Duck duck2 = new Duck("Билли", 4);
        Duck duck3 = new Duck("Вилли", 5);
        Duck duck4 = new Duck("Дилли", 4);
        Duck duck5 = new Duck("Поночка", 3);
        aviary1.addToAviary(duck1);
        aviary1.addToAviary(duck2);
        aviary1.addToAviary(duck3);
        aviary1.addToAviary(duck4);
        aviary1.addToAviary(duck5);

        bear2.eat(new Grass("Овес", 3)); // возникает исключение WrongFoodException()
    }
}
