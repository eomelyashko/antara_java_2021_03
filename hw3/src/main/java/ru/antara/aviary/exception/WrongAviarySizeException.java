package ru.antara.aviary.exception;

public class WrongAviarySizeException extends Exception {
    public WrongAviarySizeException(String msg) {
        super(msg);
    }
}
