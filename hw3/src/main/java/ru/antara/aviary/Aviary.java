package ru.antara.aviary;

import ru.antara.animals.*;
import ru.antara.aviary.exception.WrongAviarySizeException;

import java.util.HashMap;

public class Aviary<T extends Animal> {
    private HashMap<String, T> var = new HashMap<>();
    private AviarySize aviarySize;

    public Aviary(AviarySize aviarySize) {
        this.aviarySize = aviarySize;
    }

    public T addToAviary(T animal) {
        if (!(aviarySize.ordinal() >= animal.getAviarySize().ordinal())) {
            try {
                throw new WrongAviarySizeException("Воспользуйтесь вольером большего размера!");
            } catch (WrongAviarySizeException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return var.put(animal.toString(), animal);
    }

    public T outOfTheAviary(T animal) {
        return var.remove(animal.toString());
    }

    public T getAnimal(String name) {
        return var.get(name);
    }
}
