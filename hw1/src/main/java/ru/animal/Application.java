package ru.animal;

import ru.animal.model.Kotik;

public class Application {

    public static void main(String[] args) {
        Kotik tom = new Kotik(7);
        tom.setMeow("Мау!");
        Kotik matroskin = new Kotik().setKotik(8, "Матроскин", 3, "Мрррау!");
        matroskin.liveAnotherDay();
        System.out.println("Кот по имени " + matroskin + " весит " + matroskin.getWeight() + " кг");
        System.out.println(tom.getMeow().equals(matroskin.getMeow()) ? "Котики мяучат одинаково" : "Котики мяучат по-разному");
        System.out.println("Количество котиков: " + Kotik.getCatCount());
    }
}
