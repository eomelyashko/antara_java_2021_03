package ru.animal.model;

public class Kotik {

    private static int catCount;

    private String name;
    private int weight;
    private int age;
    private String meow;
    private int prettiness;

    private int fed;

    public Kotik() {
        this(10);
    }

    public Kotik(int fed) {
        catCount++;
        this.fed = fed;
    }

    public Kotik setKotik(int prettiness, String name, int weight, String meow) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
        return this;
    }

    public static int getCatCount() {
        return catCount;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public int getAge() {
        return age;
    }

    public String getMeow() {
        return meow;
    }

    public int getPrettiness() {
        return prettiness;
    }

    public int getFed() {
        return fed;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setMeow(String meow) {
        this.meow = meow;
    }

    public void setPrettiness(int prettiness) {
        this.prettiness = prettiness;
    }

    public boolean play() {
        if (isFed()) {
            fed--;
            return true;
        }
        return false;
    }

    public boolean sleep() {
        if (isFed()) {
            fed--;
            return true;
        }
        return false;
    }

    public boolean chaseMouse() {
        if (isFed()) {
            fed--;
            return true;
        }
        return false;
    }

    public boolean bounce() {
        if (isFed()) {
            fed--;
            return true;
        }
        return false;
    }

    public boolean toilet() {
        if (isFed()) {
            fed--;
            return true;
        }
        return false;
    }

    public String eat(int fed) {
        if (!isFed()) {
            this.fed += fed;
            return "Поел!";
        }
        return "Котик сыт!"; // чтобы котик не переедал, а то лопнет))
    }

    public String eat(int fed, String eatName) {
        if (!isFed()) {
            this.fed += fed;
            return "Съел " + eatName;
        }
        return "Котик сыт!";
    }

    public String eat() {
        return eat(10, "Вискас");
    }

    public boolean isFed() {
        return fed > 0;
    }

    public void liveAnotherDay() {
        for (int i = 0; i < 24; i++) {
            String s = "";
            switch (rnd(5)) {
                case 1:
                    s = play() ? "Поиграл" : eat();
                    break;
                case 2:
                    s = sleep() ? "Zzz" : eat();
                    break;
                case 3:
                    s = chaseMouse() ? "Погнался за мышкой" : eat();
                    break;
                case 4:
                    s = bounce() ? "Прыгнул" : eat();
                    break;
                case 5:
                    s = toilet() ? "Сходил в туалет" : eat();
                    break;
            }
            System.out.println(s);
        }
    }

    private int rnd(int count) {
        return (int) (Math.random() * count) + 1;
    }

    @Override
    public String toString() {
        return name;
    }
}
